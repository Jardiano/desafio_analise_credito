# desafio-analise-credito
A Aplicação foi desenvolvida usando springboot(back end), thymeleaf(front) e Mysql(BD).

# Requisitos

- java 8
- maven
- banco de dados mysql

# Configuração
> Banco de dados

Antes da execução da aplicação é necessário executar o script localizado no diretório [/src/main/resources/scripts/script.sql]
o usuário e senha do banco estão definidos no arquivo application.properties.

>Aplicação

Dentro diretório raiz do projeto, executar o comando:
```sh
mvn clean install
```

Após a geração do arquivo .jar na pasta target, basta executar o comando:
```sh
java -jar desafio_analise_credito-1.0.0-SNAPSHOT.jar.jar
```

A aplicação está acessível na url http://localhost:8080/login

Os usuário e senha no banco de dados são:

captador -  Usuário de captação de propostas de novos cartões: este usuário será responsável pelo cadastro de novos portadores de cartão.  Após o cadastro dos portadores, este usuário terá permissão apenas de verificar o resultado das análises realizadas pelos analistas

analista - Usuário analista de crédito: este usuário será responsável por verificar as propostas cadastradas, podendo aprovar ou negar a concessão de crédito.
 
 Para ambos a senha padrão é 'pass'.

# Funcionalidades

Após o login é apresentada uma tela com duas opções:

Realizar Análise da proposta - Que somente o usuário com o perfil analista consegue acessar para realizar a análise e alterar o status de uma proposta.

Visualização e Cadastro de propostas - Que somente o usuário com perfil captador consegue acessar para cadastrar propsotas e visualizar  as propostas pendentes de análise.
