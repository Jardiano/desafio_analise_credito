create database analise_credito;

use analise_credito;

drop table if exists users;
drop table if exists authorities;
drop table if exists proposta;

CREATE TABLE users (
  username VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  enabled TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (username)
);

CREATE TABLE authorities (
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (username) REFERENCES users(username)
);

CREATE UNIQUE INDEX ix_auth_username
  on authorities (username,authority);

CREATE TABLE proposta (
 id INTEGER NOT NULL auto_increment,
 data_cadastro DATETIME NOT NULL,
 descricao VARCHAR(255) NOT NULL,
 status VARCHAR(255) NOT NULL,
 valor DOUBLE PRECISION NOT NULL,
 PRIMARY KEY (id));

INSERT INTO users (username, password, enabled) values ('analista', '$2a$10$8.UnVuG9HHgffUDAlk8qfOuVGkqRzgVymGe07xd00DMxs.AQubh4a', 1);

INSERT INTO users (username, password, enabled) values ('captador', '$2a$10$8.UnVuG9HHgffUDAlk8qfOuVGkqRzgVymGe07xd00DMxs.AQubh4a', 1);

INSERT INTO authorities (username, authority) values ('analista', 'ROLE_ANALISTA');

INSERT INTO authorities (username, authority) values ('captador', 'ROLE_CAPTADOR');

insert into proposta(data_cadastro,descricao,status,valor) values(now(), 'teste em analise', 'EM_ANALISE',130);
insert into proposta(data_cadastro,descricao,status,valor) values(now(), 'teste aprovada', 'APROVADA',200);
insert into proposta(data_cadastro,descricao,status,valor) values(now(), 'teste recusada', 'RECUSADA',400);