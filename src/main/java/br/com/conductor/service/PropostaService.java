package br.com.conductor.service;

import br.com.conductor.dataaccessobject.PropostaRepository;
import br.com.conductor.datatransferobject.PropostaDTO;
import br.com.conductor.domianobject.Proposta;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface PropostaService {

    Proposta cadastrarProposta(Proposta proposta);

    boolean isPropostaAlterada(Proposta proposta);

    boolean isPropostaRemovida(Integer id);

    List<Proposta> listaPropostas();

    Proposta recuperaPorId(Integer id);

}
