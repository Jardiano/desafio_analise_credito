package br.com.conductor.service;

import br.com.conductor.dataaccessobject.PropostaRepository;
import br.com.conductor.domianobject.Proposta;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PropostaServiceImpl implements PropostaService {

    @Autowired
    private PropostaRepository propostaRepository;


    @Override
    public Proposta cadastrarProposta(Proposta proposta) {
        return propostaRepository.save(proposta);
    }


    @Override
    public boolean isPropostaAlterada(Proposta proposta) {
        Optional<Proposta> optionalProposta = propostaRepository.findById(proposta.getId());

        if (optionalProposta.isPresent()) {
            propostaRepository.save(proposta);
            return true;
        }

        return false;
    }


    @Override
    public boolean isPropostaRemovida(Integer id) {
        Proposta proposta = Proposta.builder().id(id).build();
        propostaRepository.delete(proposta);
        return true;
    }


    @Override
    public List<Proposta> listaPropostas() {
        return propostaRepository.findAll();
    }


    @Override
    public Proposta recuperaPorId(Integer id) {
        Optional<Proposta> propostaOptional = propostaRepository.findById(id);
        if (propostaOptional.isPresent()) {
            return propostaOptional.get();
        }
        return null;
    }
}
