package br.com.conductor.controller;

import br.com.conductor.domianobject.Proposta;
import br.com.conductor.service.PropostaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class WebController {

    @Autowired
    private PropostaService propostaService;


    @ModelAttribute(value = "propostas", binding = true)
    public List<Proposta> listarPropostas() {
        return propostaService.listaPropostas();
    }


    @GetMapping("/")
    public String home1() {
        return "/home";
    }


    @GetMapping("/home")
    public String home() {
        return "/home";
    }


    @GetMapping("/admin")
    public String admin(Model model) {
        return "/admin";
    }


    @GetMapping("/user")
    public String user(Model model) {
        return "/user";
    }


    @GetMapping("/about")
    public String about() {
        return "/about";
    }


    @GetMapping("/login")
    public String login() {
        return "/login";
    }


    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }


    @GetMapping("/cadastrar-proposta")
    public String createProjectForm(Model model) {

        model.addAttribute("proposta", new Proposta());
        return "/cadastroProposta";
    }


    @GetMapping("/analisar-proposta/{id}")
    public String updateProjectForm(Model model, @PathVariable("id") Integer id) {
        Proposta proposta = propostaService.recuperaPorId(id);
        model.addAttribute("proposta", proposta);
        return "/analiseProposta";
    }


    @PostMapping("/salvar-proposta")
    public String saveProjectSubmission(@ModelAttribute Proposta proposta) {
        if(proposta.getId() == null){
            proposta = propostaService.cadastrarProposta(proposta);
        }

        return "/user";
    }

    @PostMapping("/atualizar-proposta")
    public String atualizarProposta(@ModelAttribute Proposta proposta) {

        if(proposta.getId() != null){
            propostaService.cadastrarProposta(proposta);
        }

        return "/admin";
    }
}
