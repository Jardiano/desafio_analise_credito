package br.com.conductor.dataaccessobject;

import br.com.conductor.domianobject.Proposta;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PropostaRepository extends CrudRepository<Proposta, Integer> {

    List<Proposta> findAll();

}
